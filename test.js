require("chai").should();

describe("cssv", function cssvTest() {
  it("should convert the variables to text", function convertVariables() {
    require("./cssv")([
      "@define myVar = 'Open Sans'",
      "body {",
      "font-family: $myVar;;",
      "}"
    ]).should.deep.equal([
      "body {",
      "font-family: 'Open Sans';",
      "}"
    ]);
  });

  it("should fail when given an invalid variable", function failOnBadVariable() {
    (function failTester() {
      require("./cssv")([
        "@define myVar = 'Open Sans'",
        "body {",
        "font-family: $myvar;;",
        "}"
      ]);
    }).should.throw(ReferenceError, "[cssv] Usage of undefined variable myvar");
  });

  it("should fail when given a variable declaration without a name", function failOnBadDecl() {
    (function failDeclTester() {
      require("./cssv")([
        "@define = sans-serif"
      ]);
    }).should.throw(SyntaxError, "[cssv] On line 1: Invalid variable declaration");
  });
});
