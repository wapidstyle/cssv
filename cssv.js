/*
 * CSS with Variables 2.1
 */
// Ooh, colors!
const fs = require("graceful-fs");
const argv = require("minimist")(process.argv.slice(2));
const winston = require("winston");
const runningAsScript = !module.parent;
const _ = {
  isString: require("lodash.isString")
};

let logger = null;

if (runningAsScript) {
  // Called directly
  winston.loggers.add("main", {
    console: {
      level: "info",
      colorize: true,
    }
  });
  logger = winston.loggers.get("main");
  if (argv._[0] === undefined) {
    logger.error("Input file not specified, exiting!");
    process.exit(-1);
  }
  if (argv._[1] === undefined) {
    logger.warn("No output file specified, using " + argv._[0].split(".")[0] + ".css");
  }
} else {
  winston.loggers.add("cssv", {
    console: {
      level: "info",
      colorize: true,
      label: "cssv",
    }
  });
  logger = winston.loggers.get("cssv");
}

const debug = logger.debug;
const finer = logger.silly;

if (argv.debug) {
  logger.transports.console.level = "debug";
  logger.debug("Welcome to debug mode!");
}
if (argv.finer) {
  logger.transports.console.level = "silly";
  logger.silly("Welcome to finer mode");
}

finer("Arguments: " + argv._);

function _process(input) {
  let split = [];
  if (typeof input !== String) {
    split = input;
  }
  // Begin checking of file
  let vars = [];
  let output = [];
  // If the line format is \r\n, then adjust accordingly
  if (_.isString(input) && input.split("\n")[0].endsWith("\r")) {
    split = input.split("\r\n");
  } else if (_.isString(input)) {
    split = input.split("\n");
  }
  finer("Vars declared, starting forEach");
  // For every line
  split.forEach(function lineLoop(line, index) {
    finer("Line is \"" + line + "\", index " + index);
    // If it is a variable declaration
    if (line.startsWith("@define")) {
      finer("Line is a variable decl");
      const splitLine = line.split(" ");
      if (splitLine[1].indexOf("=") > 0) {
        const splitLineEqual = splitLine[1].split("=");
        let val = splitLineEqual[1];
        // TODO Put the following code into a seperate function
        if (splitLineEqual[1].startsWith("'") || splitLineEqual[1].startsWith("\"")) {
          val += " ";
          let done = false;
          finer("Split line by =, beginning loop");
          splitLine.forEach(function splitByEquals(split, splitIndex) {
            if (splitIndex >= 3 && !done) {
              if (split.endsWith("'") || split.endsWith("\"")) {
                val += split;
                done = true;
              } else {
                val += split + " ";
              }
            }
          });
        }
        debug("Initalized " + splitLineEqual[0] + " as " + val + " via method 1");
        vars[splitLineEqual[0]] = val;
      } else {
        if (splitLine[2] == "=") {
          let valu = splitLine[3];
          if (splitLine[3].startsWith("'") || splitLine[3].startsWith("\"")) {
            valu += " ";
            let done = false;
            splitLine.forEach(function splitBySpaces(splittedLine, lineIndex) {
              if (lineIndex >= 4 && !done) {
                if (splittedLine.endsWith("'") || splittedLine.endsWith("\"")) {
                  valu += splittedLine;
                  done = true;
                } else {
                  valu += splittedLine + " ";
                }
              }
            });
          }
          debug("Initalized " + splitLine[1] + " as " + valu + " via method 2");
          vars[splitLine[1]] = valu;
        } else {
          // TODO Repair this to recognize more things, and warn if it recognizes them.
          // TODO Make a custom error setup, maybe cssv.InvalidVariableDeclare?
          // Throws a SyntaxError because it make sense in the context. :O
          throw SyntaxError("[cssv] On line " + (index + 1) + ": Invalid variable declaration");
        }
      }
      // In an else because a declaration line should not reference a variable.
    } else {
      // If it references a variable...
      if (line.indexOf("$") > -1) {
        finer("Line references a variable");
        const lne = line.split(""); // Wow.
        let inVar = false;
        let varName = "";
        let begin = 0;
        let end = 0;
        let valid = false;
        let wasValid = false;

        lne.forEach(function splitLineLoop(splitLine, char) {
          if (splitLine == "$") {
            finer("Found beginning of var!");
            if (inVar === false) {
              begin = Number(char);
              inVar = true;
              varName = "";
            } else {
              inVar = false;
              varName = "";
            }
          } else {
            if (inVar === true) {
              if (splitLine == "$" || splitLine === undefined) {
                end = char - 1;
                lne.splice(begin, end);
                inVar = false;
                end = Number(char);
              } else {
                if (valid === false && wasValid === true) {
                  inVar = false;
                  debug("Reference to " + varName + " on line " + (line + 1) + " via method 1");
                  line.replace("$" + varName, vars[varName]);
                  varName = "";
                }
                if (splitLine.endsWith(";") === true || splitLine.endsWith(",") === true) {
                  inVar = false;
                  if (vars[varName] === undefined) {
                    throw new ReferenceError("[cssv] Usage of undefined variable " + varName);
                  }
                  debug("Reference to " + varName + " on line " + (line + 1) + " via method 2");
                  line = line.replace("$" + varName + ";", vars[varName]);
                  varName = "";
                }
                varName += splitLine;
                if (varName == "," || varName == ";") {
                  varName = "";
                }
                if (valid === true) {
                  wasValid = true;
                }
                if (varName.endsWith(";") === false && varName.endsWith(",") === false && vars[varName]) {
                  valid = true;
                }
              }
            }
          }
        });
      }
    }
    if (!line.startsWith("@define") && line !== "\n") {
      finer("Done line!");
      output.push(line);
    } else {
      finer("Ignored putting line in output");
    }
  });
  return output;
}

// Check if the input is a valid file
function cssv(inputFile, outputFile) {
  /*
    +-----------------------+
    |      T E S T S        |
    +-----------------------+
  */
  //throw TypeError("Hello World"); // Tests error handler
  //throw Error(); // Tests "no message"
  //throw null; // Tests "unknown error"
  if (!_.isString(inputFile)) {
    finer("Returning processed data");
    return _process(inputFile);
  } else {
    finer("Starting FS stat");
    try {
      if (fs.statSync(inputFile).isFile()) {
        finer("Is file!");
        const data = fs.readFileSync(inputFile, "utf-8");
        finer("Writing to file!");
        fs.writeFileSync(outputFile, _process(data).join("\n"));
        if (require.name === module) {
          logger.info("All done!");
        }
      } else {
        finer("Is not a file");
        logger.error("The input is not a file, is it a folder?");
        process.exit(-4);
      }
    } catch (e) {
      if (runningAsScript) {
        if (e.message) {
          logger.error("An error has occured.");
          if (e.message === "") {
            logger.error(e.name + ": No message provided. This " +
                            "was most likely done for debug.");
          } else if (_.isString(e.message)) {
            logger.error(e.name + ": " + e.message);
            process.exit(-3);
          } else {
            logger.error("An unknown error has occured. Please file a bug report.");
            logger.error(e.name + ": " + e.message);
            process.exit(-10);
          }
        }
      } else {
        throw e;
      }
    }
  }
}

finer("Running.");
if (runningAsScript) {
  // Called directly
  cssv(argv._[0], argv._[1] || argv._[0].split(".")[0] + ".css");
} else {
  // Required
  finer("Exposing cssv()");
  module.exports = cssv;
}
