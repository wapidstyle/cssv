# NOTE
**This project is pretty much discontinued.** There's a lot to be done, but it
works, and as it's 3 years old I'm discontinuing it. It's let me learn how to
use Chai and Mocha and fix up old code. For example, for whatever reason
I had this `git` repo for a long time, but no commits. I had the code of 2403d40
and thought, *great, this needs* **another** *rewrite*. Heck, it's
[in the code](http://preview.tinyurl.com/zk9h3ko). **If you want to work on it,
go ahead. I'd love to see what people do with it.**

Thank you for reading that. Now, ONTO THE `README`!
# CSSV
## CSS, but with variables
One of the large reasons that major projects use
less.css and SCSS is for one reason: variables.
It's a huge help for the creators of the project
to only specify the default things, and then let
the users do the rest. CSSV allows you to code in
full CSS, however have variables at your command.
No loops. No fancy stuff. Just variables. If you
want all that extra stuff, find another
preprocessor. Don't expect it to be in this one.
## Cloning
To get the latest, do the usual command. Otherwise, do:
```shell
git clone https://bitbucket.org/wapidstyle/cssv.git --tag 2.1.3
```
To get 2.1.0.
## Usage
Using it is quite simple. Use `$<variable name>;` to reference it:
```cssv
@define color = red
body {
  color: $color;;
}
```
That's it. Nothing else. However, **remember to
end a variable's name with a semicolon.** The input would
output:
```css
body {
  color: red;
}
```
