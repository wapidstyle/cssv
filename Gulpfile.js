const gulp = require("gulp");

gulp.task("default", function defaultTask() {
  console.log("--------------------[cssv]--------------------");
  console.log("gulp jshint         Lints cssv with JSHint");
  console.log("\"    jscs           Lints cssv with JSCS");
  console.log("\"    mocha          Runs unit tests on cssv");
  console.log("");
  console.log("gulp lint           Runs JSHint and JSCS");
  console.log("\"    test           Runs Mocha");
});

gulp.task("jshint", function jshintTask() {
  const jshint = require("gulp-jshint");
  return gulp.src("./cssv.js")
    .pipe(jshint())
    .pipe(jshint.reporter(require("jshint-stylish")))
    .pipe(jshint.reporter("fail"));
});

gulp.task("jscs", function jscsTask() {
  const jscs = require("gulp-jscs");
  return gulp.src("./cssv.js")
    .pipe(jscs())
    .pipe(jscs.reporter("jshint-stylish"))
    .pipe(jscs.reporter("fail"));
});

gulp.task("mocha", function mochaTask() {
  return gulp.src("test.js", {read: false})
    .pipe(require("gulp-mocha")());
});

gulp.task("lint", ["jshint", "jscs"], function() {});
